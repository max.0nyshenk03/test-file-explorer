import { isAfter } from "date-fns";
import { v4 } from "uuid";

export const genUUID = () => v4();

export const isDateAfter = (firstDate: Date, secondDate: Date): boolean =>
  isAfter(firstDate, secondDate);
