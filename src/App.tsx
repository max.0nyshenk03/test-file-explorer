import { useState } from "react";
import {
  Box,
  Breadcrumbs,
  Button,
  Dialog,
  DialogTitle,
  FormControlLabel,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Switch,
  TextField,
  Typography,
} from "@mui/material";
import {
  Folder as FolderIcon,
  Delete as DeleteIcon,
} from "@mui/icons-material";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import { DatePicker, LocalizationProvider } from "@mui/lab";
import { genUUID, isDateAfter } from "./utils";

type File = {
  id: string;
  name: string;
  creationDate: Date;
  nested: File[];
  folder: boolean;
  parentFolderId: string;
};

type FileMode = "" | "folder" | "file";

const createEmptyFolder = () => ({
  id: genUUID(),
  name: "",
  creationDate: new Date(),
  parentFolderId: "",
  nested: [],
  folder: true,
});

export default function App() {
  const [files, setFiles] = useState<File[]>([]);
  const [createFileMode, setCreateFileMode] = useState<FileMode>("");
  const [fileName, setFileName] = useState("");
  const [currentFolder, setCurrentFolder] = useState<File>(createEmptyFolder());
  const [modifiedFile, setModifiedFile] = useState<File | null>(null);
  const [filters, setFilters] = useState<{
    showFoldersOnly: boolean;
    date: Date | null;
  }>({ showFoldersOnly: false, date: null });
  const [breadcrumbs, setBreadcrumbs] = useState<string[]>([]);
  const isRootLevel = !findFile(currentFolder.id, files);

  const createFile = ({
    fileName,
    fileMode,
    parentFolderId,
  }: {
    fileName: string;
    fileMode: FileMode;
    parentFolderId: string;
  }) => {
    const updatedCurrentFolder = {
      ...currentFolder,
      nested: [
        ...currentFolder.nested,
        {
          ...createEmptyFolder(),
          name: fileName,
          folder: fileMode === "folder",
          parentFolderId,
        },
      ],
    };
    setCurrentFolder(updatedCurrentFolder);
    setFiles(
      isRootLevel
        ? updatedCurrentFolder.nested
        : fileTreeMap(files, (file) =>
            file.id === updatedCurrentFolder.id ? updatedCurrentFolder : file
          )
    );
  };

  const changeFile = (file: File) => {
    const updatedCurrentFolder = {
      ...currentFolder,
      nested: currentFolder.nested.map((f) => (f.id === file.id ? file : f)),
    };
    setFiles(
      isRootLevel
        ? updatedCurrentFolder.nested
        : fileTreeMap(files, (f) => (f.id === file.id ? file : f))
    );
    setCurrentFolder(updatedCurrentFolder);
  };

  const goLevelUp = () => {
    const folder = findFile(currentFolder.parentFolderId, files);
    setCurrentFolder(
      folder || {
        ...createEmptyFolder(),
        nested: files,
        id: files[0]?.parentFolderId,
      }
    );
    setBreadcrumbs(breadcrumbs.slice(0, -1));
  };

  const deleteFile = (id: string) => {
    const updatedCurrentFolder = {
      ...currentFolder,
      nested: currentFolder.nested.filter((f) => f.id !== id),
    };
    setFiles(
      isRootLevel
        ? updatedCurrentFolder.nested
        : filterFileTree(files, (f) => f.id !== id)
    );
    setCurrentFolder(updatedCurrentFolder);
  };

  return (
    <Box sx={{ p: 4 }}>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Box display="flex" alignItems="center">
          <Button
            sx={{ mr: 3 }}
            variant="contained"
            onClick={() => setCreateFileMode("file")}
          >
            Create file
          </Button>
          <Button
            color="secondary"
            variant="contained"
            onClick={() => setCreateFileMode("folder")}
          >
            Create folder
          </Button>
        </Box>
        <Box display="flex" alignItems="center">
          <FormControlLabel
            sx={{ ml: 4 }}
            control={
              <Switch
                checked={filters.showFoldersOnly}
                onChange={() =>
                  setFilters({
                    ...filters,
                    showFoldersOnly: !filters.showFoldersOnly,
                  })
                }
              />
            }
            label="Show folders only"
          />
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DatePicker
              label="Creation date filter"
              value={filters.date}
              onChange={(filterDate) =>
                setFilters({ ...filters, date: filterDate })
              }
              renderInput={(params) => <TextField {...params} />}
            />
          </LocalizationProvider>
        </Box>
      </Box>

      <Breadcrumbs sx={{ minHeight: 30 }} aria-label="breadcrumb">
        {breadcrumbs.map((breadcrumb) => (
          <Typography>{breadcrumb}</Typography>
        ))}
      </Breadcrumbs>

      <List sx={{ border: "1px solid lavender" }}>
        {currentFolder.parentFolderId && (
          <ListItem disablePadding>
            <ListItemButton onClick={goLevelUp}>../</ListItemButton>
          </ListItem>
        )}
        {currentFolder.nested
          .filter(
            (f) =>
              (filters.showFoldersOnly
                ? f.folder === filters.showFoldersOnly
                : true) &&
              (filters.date ? isDateAfter(f.creationDate, filters.date) : true)
          )
          .map((file) => (
            <ListItem
              key={file.id}
              disablePadding
              secondaryAction={
                <>
                  <Button
                    variant="outlined"
                    onClick={() => setModifiedFile(file)}
                  >
                    Rename
                  </Button>
                  <IconButton
                    edge="end"
                    sx={{ ml: 3 }}
                    onClick={() => deleteFile(file.id)}
                    aria-label="delete"
                  >
                    <DeleteIcon />
                  </IconButton>
                </>
              }
            >
              <ListItemButton
                onClick={() => {
                  if (!file.folder) return;
                  setCurrentFolder(file);
                  setBreadcrumbs([...breadcrumbs, file.name]);
                }}
              >
                <ListItemIcon>{file.folder && <FolderIcon />}</ListItemIcon>
                <ListItemText
                  secondary={file.creationDate.toISOString()}
                  primary={file.name}
                />
              </ListItemButton>
            </ListItem>
          ))}
      </List>

      <Dialog onClose={() => setCreateFileMode("")} open={!!createFileMode}>
        <DialogTitle>Create {createFileMode}</DialogTitle>
        <TextField
          onChange={(e) => setFileName(e.target.value)}
          value={fileName}
        />
        <Button
          onClick={() => {
            createFile({
              fileName,
              fileMode: createFileMode,
              parentFolderId: currentFolder.id,
            });
            setFileName("");
            setCreateFileMode("");
          }}
        >
          Create
        </Button>
        <Button
          onClick={() => {
            setFileName("");
            setCreateFileMode("");
          }}
        >
          Cancel
        </Button>
      </Dialog>
      <Dialog onClose={() => setModifiedFile(null)} open={!!modifiedFile}>
        <DialogTitle>Enter a new name</DialogTitle>
        {modifiedFile && (
          <>
            <TextField
              onChange={(e) =>
                setModifiedFile({ ...modifiedFile, name: e.target.value })
              }
              value={modifiedFile.name}
            />
            <Button
              onClick={() => {
                changeFile(modifiedFile);
                setModifiedFile(null);
              }}
            >
              Rename
            </Button>
            <Button onClick={() => setModifiedFile(null)}>Cancel</Button>
          </>
        )}
      </Dialog>
    </Box>
  );
}

const findFile = (id: string, fileTree: File[]): File | undefined => {
  return (
    fileTree.find((file) => file.id === id) ||
    fileTree.reduce((acc: File | undefined, file) => {
      if (file.nested) {
        const foundFile = findFile(id, file.nested);
        foundFile && (acc = foundFile);
      }
      return acc;
    }, undefined)
  );
};

const fileTreeMap = (tree: File[], cb: (item: File) => File): File[] => {
  return tree.reduce((acc: File[]) => {
    acc = acc.map((f) => {
      f = cb(f);
      f.nested.length && (f.nested = fileTreeMap(f.nested, cb));
      return f;
    });

    return acc;
  }, tree);
};

const filterFileTree = (tree: File[], cb: (item: File) => boolean): File[] =>
  tree.filter((f) => {
    const res = cb(f);
    if (res) {
      f.nested = filterFileTree(f.nested, cb);
    }
    return res;
  });
